{{/*
Expand the name of the chart.
*/}}
{{- define "releases-watcher.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}


{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "releases-watcher.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "releases-watcher.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}


{{/*
Common labels
*/}}
{{- define "releases-watcher.labels" -}}
helm.sh/chart: {{ include "releases-watcher.chart" . }}
{{ include "releases-watcher.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- include "releases-watcher.userDefinedLabels" . }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "releases-watcher.selectorLabels" -}}
app.kubernetes.io/name: {{ include "releases-watcher.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}


{{/*
User Defined labels
*/}}
{{- define "releases-watcher.userDefinedLabels" -}}
{{- if .Values.userDefinedLabels}}
{{- toYaml .Values.userDefinedLabels }}
{{- end }}
{{- end }}
