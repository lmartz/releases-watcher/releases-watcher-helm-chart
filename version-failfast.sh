#!/bin/bash

CHART_VERSION=${1}
FILE_CHART_VERSION=${2}
LAST_UPLOADED_VERSION=${3}

echo "Checking that the Chart version defined in the CI matches the one in the Chart.yaml"

if [ "${CHART_VERSION}" == "${FILE_CHART_VERSION}" ]
    then
        echo "Checking that the Chart version defined in the CI is different from the last uploaded one"

        if [ "${CHART_VERSION}" != "${LAST_UPLOADED_VERSION}" ]
            then
                exit 0
            else
                echo "The Chart version you are trying to push ( ${CHART_VERSION} )  is the same as the last uploaded version ( ${CHART_VERSION} )."
                exit 1
        fi
    else
        echo "The CI declared chart version ( ${CHART_VERSION} ) does not match the one in the chart file ( ${FILE_CHART_VERSION} )."
        exit 1
fi
